﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public GameObject mainPanel, creditsPanel ;


    // Start is called before the first frame update
    void Start()
    {
        mainPanel.SetActive(true);
        creditsPanel.SetActive(false);
    }


    public void startGame()
    {

    }

    public void startTutorial()
    {
        SceneManager.LoadScene(1);
    }

    public void returnMenu()
    {
        mainPanel.SetActive(true);
        creditsPanel.SetActive(false);

    }

    public void showCredits()
    {
        mainPanel.SetActive(false);
        creditsPanel.SetActive(true);
    }

    public void quitGame()
    {
        Application.Quit();
    } 

}
