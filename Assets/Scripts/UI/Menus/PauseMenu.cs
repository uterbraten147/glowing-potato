﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject panelPauseMenu;

    // Start is called before the first frame update
    void Awake()
    {
        panelPauseMenu.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GamePaused()
    {
        panelPauseMenu.SetActive(true);
    }

    public void returnGame()
    {
        panelPauseMenu.SetActive(false);
        //tiempo
    }

    public void returnMainMenu()
    {
        //escena 0
        SceneManager.LoadScene(0);
    }
}
