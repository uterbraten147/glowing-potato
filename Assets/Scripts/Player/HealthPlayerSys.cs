﻿using UnityEngine;

public class HealthPlayerSys : MonoBehaviour
{
    [SerializeField] private float MaxLife;

    private float currentLife;

    private void Start()
    {
        InitStats();
    }

    private void Update()
    {
        RegenerarVida();
    }

    public void InitStats()
    {
        currentLife = MaxLife;
    }

    public void TakeDamage(float _damage)
    {
        currentLife -= _damage;
    }

    public void CheckHealth()
    {
        if (currentLife >= MaxLife)
        {
            currentLife = MaxLife;
        }

        if (currentLife <= 0)
        {
            currentLife = 0;
            Debug.Log("Muerto");
        }
    }

    public void RegenerarVida()
    {
        if (currentLife < MaxLife && currentLife > 0)
        {
            Invoke(nameof(Curar), 4);
        }

        CheckHealth();
    }

    public void Curar()
    {
        currentLife += 5f;
    }

    public float getLife()
    {
        return currentLife;
    }

    public float getMaxLife()
    {
        return MaxLife;
    }
}