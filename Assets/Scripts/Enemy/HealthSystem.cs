﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthSystem : MonoBehaviour
{
    public float enemyLife;
    float maxVida;

    
    // Start is called before the first frame update
    void Start()
    {
        maxVida = enemyLife;
    }

    void checkLife()
    {
        if(maxVida <= 0)
        {
            Destroy(this.gameObject);
        }
    }

   
    public void enemyDamage(float _dmg)
    {
        if(maxVida > 0)
        {
            maxVida -= _dmg;
            checkLife();
        }        

    }
}
