﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    public NavMeshAgent agent;

    public Transform player;

    public LayerMask isGround, isPlayer;


    //Atacar
    public float timeBetweenAttacks;
    public GameObject projectile;
    bool isAttacking;

    //rangos
    public float sightRange, attackRange;
    public bool playerAttackRange, playerVistoRange;

    void Awake()
    {
        player = GameObject.Find("Player").transform;
        agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        //checar rango del player
        playerVistoRange = Physics.CheckSphere(transform.position, sightRange, isPlayer);
        playerAttackRange = Physics.CheckSphere(transform.position, sightRange, isPlayer);

        if (playerVistoRange && !playerAttackRange) chasePlayer();
        if (playerVistoRange && playerAttackRange) attackPlayer();

    }

    void chasePlayer()
    {
        agent.SetDestination(player.position);
    }

    void attackPlayer()
    {
        transform.LookAt(player);

        if (!isAttacking)
        {
            
            //Logica de enemigo que lanza cosas
            Rigidbody rb = Instantiate(projectile, transform.position, Quaternion.identity).GetComponent<Rigidbody>();
            rb.AddForce(transform.forward * 32f, ForceMode.Impulse);
            rb.AddForce(transform.up * 8f, ForceMode.Impulse);

            isAttacking = true;
            Invoke(nameof(ResetAttack), timeBetweenAttacks);
        }
    }

    void ResetAttack()
    {
        isAttacking = false;

    }
}
