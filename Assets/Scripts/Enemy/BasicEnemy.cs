﻿using UnityEngine;
using UnityEngine.AI;

public class BasicEnemy : MonoBehaviour
{
    [SerializeField] private float damage;
    private float lastAttackTime = 0f;
    private float attackCoolDown = 2f;

    [SerializeField] private float stopDist;

    private NavMeshAgent agent;
    private GameObject player;

    // Start is called before the first frame update
    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    private void Update()
    {
        float dist = Vector3.Distance(transform.position, player.transform.position);

        if (dist < stopDist)
        {
            // hacer daño al player
            stopEnemy();
        }
        else
        {
            goToPlayer();
        }
    }

    private void goToPlayer()
    {
        agent.isStopped = false;
        agent.SetDestination(player.transform.position);
    }

    private void stopEnemy()
    {
        agent.isStopped = true;
    }

    private void Attack()
    {
        if (Time.time - lastAttackTime >= attackCoolDown)
        {
            lastAttackTime = Time.time;
            player.GetComponent<HealthPlayerSys>().TakeDamage(damage);
        }
    }
}